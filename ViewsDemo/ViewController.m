//
//  ViewController.m
//  ViewsDemo
//
//  Created by James Cash on 12-06-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic,strong) UIView* v1;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    self.v1 = [[UIView alloc] initWithFrame:CGRectMake(100, 20, 150, 150)];
    self.v1.backgroundColor = UIColor.purpleColor;

    [self.view addSubview:self.v1];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    NSLog(@"1.view frame %@", NSStringFromCGRect(self.v1.frame));
    [UIView animateWithDuration:2 animations:^{
        // try playing around with making it go up again once it hits the end, or moving other dimensions
        self.v1.frame = CGRectOffset(self.v1.frame, 0, 50);
        // try animating other properties
    } completion:^(BOOL finished) {
        NSLog(@"2. animaNtion finished %d", finished);
    }];
    NSLog(@"3. view frame %@", NSStringFromCGRect(self.v1.frame));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
